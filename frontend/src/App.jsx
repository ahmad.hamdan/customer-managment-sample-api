import React, { useEffect, useState } from "react"
import { Space, Table, Button, Input, Form, message } from "antd"

const App = () => {
	// URL for fetching customer data
	const { REACT_APP_API_URL } = process.env

	var urlVar
	if (REACT_APP_API_URL) {
		urlVar = REACT_APP_API_URL + "/customers"
	} else {
		urlVar = "http://localhost:8090/customers"
	}
	const url = urlVar

	// Table columns
	const columns = [
		{
			title: "Name",
			dataIndex: "name",
			key: "name",
		},
		{
			title: "Address",
			dataIndex: "address",
			key: "address",
		},
		{
			title: "Phone Number",
			key: "number",
			dataIndex: "number",
		},
		{
			title: "Action",
			key: "action",
			render: (_, record) => (
				<Space size='middle'>
					<Button onClick={() => handleEditOnClick(record)}>Edit</Button>
					<Button
						type='primary'
						danger
						onClick={() => handleDeleteOnClick(record.id)}
					>
						Delete
					</Button>
				</Space>
			),
		},
	]

	// State variables
	const [state, setState] = useState({
		customers: [],
		editId: null,
		forceUpdate: false,
	})

	// Fetch initial customer data
	useEffect(() => {
		fetch(url)
			.then((response) => response.json())
			.then((data) => {
				// Update key property for each customer
				const updatedData = data.content.map((element) => {
					element.key = element.id
					return element
				})
				setState((prevState) => ({ ...prevState, customers: updatedData }))
			})
			.catch((err) => {
				console.log(err.message)
				message.error("Failed to fetch customer data.")
			})
	}, [state.forceUpdate])

	// Handle form submission
	function handleSubmit(data) {
		console.log("data = " + JSON.stringify(data))
		const requestUrl = state.editId ? `${url}/${state.editId}` : url
		const method = state.editId ? "PUT" : "POST"
		fetch(requestUrl, {
			method: method,
			body: JSON.stringify(data),
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
		})
			.then(async (response) => {
				if (!response.ok) {
					const error = await response.json()
					throw new Error(error.detail)
				}
			})
			.then(() => {
				setState((prevState) => ({
					...prevState,
					editId: null,
					forceUpdate: prevState.forceUpdate === false,
				}))
				form.resetFields()
				message.success(
					state.editId
						? "Customer updated successfully."
						: "Customer added successfully."
				)
			})
			.catch((error) => {
				console.log(error)
				let errorMessage = "Failed to perform the operation."
				if (error.message != null) {
					errorMessage = error.message
				}
				message.error(errorMessage)
			})
	}

	// Handle edit button click
	function handleEditOnClick(record) {
		setState({ ...state, editId: record.id })
		const { name, address, number } = record
		form.setFieldsValue({ name, address, number })
	}

	// Handle delete button click
	function handleDeleteOnClick(id) {
		fetch(`${url}/${id}`, {
			method: "DELETE",
		})
			.then(async (response) => {
				if (!response.ok) {
					const error = await response.json()
					throw new Error(error.detail)
				}
				setState((prevState) => ({
					...prevState,
					forceUpdate: prevState.forceUpdate === false,
				}))
			})
			.catch((error) => {
				console.log(error)
				let errorMessage = "Failed to perform the operation."
				if (error.message != null) {
					errorMessage = error.message
				}
				message.error(errorMessage)
			})
	}

	const [form] = Form.useForm()

	return (
		<div>
			<Form
				form={form}
				name='basic'
				labelCol={{ span: 8 }}
				wrapperCol={{ span: 16 }}
				style={{ maxWidth: 600 }}
				initialValues={{ remember: true }}
				onFinish={handleSubmit}
				autoComplete='off'
			>
				<Form.Item label='Name: ' name='name'>
					<Input />
				</Form.Item>
				<Form.Item label='Address: ' name='address'>
					<Input />
				</Form.Item>
				<Form.Item label='Phone Number: ' name='number'>
					<Input />
				</Form.Item>
				<Form.Item wrapperCol={{ offset: 8, span: 16 }}>
					{state.editId === null ? (
						<Space>
							<Button type='primary' htmlType='submit'>
								Add Customer
							</Button>
						</Space>
					) : (
						<Space>
							<Button type='primary' htmlType='submit'>
								Edit Customer
							</Button>
							<Button
								onClick={() => {
									form.resetFields()
									setState({ ...state, editId: null })
								}}
							>
								Cancel
							</Button>
						</Space>
					)}
				</Form.Item>
			</Form>
			<Table columns={columns} dataSource={state.customers} />
		</div>
	)
}

export default App
