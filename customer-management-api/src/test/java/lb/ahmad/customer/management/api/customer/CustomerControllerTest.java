package lb.ahmad.customer.management.api.customer;

import lb.ahmad.customer.management.api.CustomerManagementApiApplication;
import lb.ahmad.customer.management.api.PostgresTestContainer;
import lb.ahmad.customer.management.api.customer.model.CustomerRequest;
import lb.ahmad.customer.management.api.customer.model.CustomerResponse;
import lb.ahmad.customer.management.api.integration.phonenumbervalidation.ValidatePhoneNumberService;
import lb.ahmad.customer.management.api.integration.phonenumbervalidation.exceptions.InvalidPhoneNumberException;
import lb.ahmad.customer.management.api.integration.phonenumbervalidation.model.ValidatePhoneNumberResponse;
import reactor.core.publisher.Mono;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = CustomerManagementApiApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerControllerTest {

	private static final Logger log = LoggerFactory.getLogger(CustomerControllerTest.class);
	private static final Map<String, Object> testCache = new HashMap<>();

	@LocalServerPort
	private int port;
	private WebTestClient webTestClient;
	@Container
	private static final PostgreSQLContainer<PostgresTestContainer> postgreSQLContainer = PostgresTestContainer
			.getInstance();
	@MockBean
	private ValidatePhoneNumberService validatePhoneNumberService;

	@BeforeEach
	void setUp() {
		this.webTestClient = WebTestClient.bindToServer()
				.baseUrl("http://localhost:" + port + CustomerController.BASE_PATH).build();
		Mockito.when(validatePhoneNumberService.validatePhoneNumber("03123456"))
				.thenReturn(Mono.just(new ValidatePhoneNumberResponse("Touch", "Lebanon", "961")));
		Mockito.when(validatePhoneNumberService.validatePhoneNumber("0312345"))
				.thenThrow(new InvalidPhoneNumberException());
	}

	@Test
	@Order(100)
	void createCustomer() {
		CustomerRequest request = new CustomerRequest();
		request.setAddress("address");
		request.setName("name");
		request.setNumber("03123456");

		log.info("request = {}", request);
		CustomerResponse response = webTestClient.post().uri(CustomerController.CREATE_CUSTOMER_ENDPOINT)
				.bodyValue(request).exchange().expectBody(CustomerResponse.class).returnResult().getResponseBody();

		log.info("response = {}", response);

		Assertions.assertNotNull(response);
		Assertions.assertNotNull(response.getId());
		Assertions.assertEquals(request.getAddress(), response.getAddress());
		Assertions.assertEquals(request.getName(), response.getName());
		Assertions.assertEquals(request.getNumber(), response.getNumber());

		testCache.put("createResponse", response);
	}

	@Test
	@Order(101)
	void createCustomerWithInvalidNumber() {
//		Mockito.doThrow(InvalidPhoneNumberException.class).when(validatePhoneNumberService)
//				.validatePhoneNumber("0312345");

		CustomerRequest request = new CustomerRequest();
		request.setAddress("address");
		request.setName("name");
		request.setNumber("0312345");

		log.info("request = {}", request);

		webTestClient.post().uri(CustomerController.CREATE_CUSTOMER_ENDPOINT).bodyValue(request).exchange()
				.expectStatus().is4xxClientError();
	}

	@Test
	@Order(200)
	void readCustomers() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");
		webTestClient.get().uri(CustomerController.READ_CUSTOMERS_ENDPOINT).exchange().expectBody()
				.jsonPath("$.content").isArray().jsonPath("$.content.length()").isEqualTo(1).jsonPath("$.content[0].id")
				.isEqualTo(createResponse.getId().toString()).jsonPath("$.content[0].name")
				.isEqualTo(createResponse.getName()).jsonPath("$.content[0].address")
				.isEqualTo(createResponse.getAddress()).jsonPath("$.content[0].number")
				.isEqualTo(createResponse.getNumber());
	}

	@Test
	@Order(300)
	void readCustomer() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");
		CustomerResponse response = webTestClient.get()
				.uri(CustomerController.READ_CUSTOMER_ENDPOINT, createResponse.getId()).exchange()
				.expectBody(CustomerResponse.class).returnResult().getResponseBody();

		log.info("response = {}", response);

		Assertions.assertNotNull(response);
		Assertions.assertEquals(createResponse, response);
	}

	@Test
	@Order(400)
	void updateCustomer() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");

		CustomerRequest updateRequest = new CustomerRequest();
		updateRequest.setName("differentName");
		updateRequest.setNumber(createResponse.getNumber());
		updateRequest.setAddress(createResponse.getAddress());

		CustomerResponse response = webTestClient.put()
				.uri(CustomerController.UPDATE_CUSTOMER_ENDPOINT, createResponse.getId()).bodyValue(updateRequest)
				.exchange().expectBody(CustomerResponse.class).returnResult().getResponseBody();

		log.info("response = {}", response);

		Assertions.assertNotNull(response);
		Assertions.assertNotEquals(createResponse, response);
		Assertions.assertEquals(updateRequest.getNumber(), response.getNumber());
		Assertions.assertEquals(updateRequest.getName(), response.getName());
		Assertions.assertEquals(updateRequest.getAddress(), response.getAddress());
	}

	@Test
	@Order(401)
	void updateCustomerWithInvalidNumber() {
//Mockito.doThrow(InvalidPhoneNumberException.class).when(validatePhoneNumberService)
//		.validatePhoneNumber("0312345");

		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");

		CustomerRequest updateRequest = new CustomerRequest();
		updateRequest.setName(createResponse.getName());
		updateRequest.setNumber("0312345");
		updateRequest.setAddress(createResponse.getAddress());

		webTestClient.put().uri(CustomerController.UPDATE_CUSTOMER_ENDPOINT, createResponse.getId()).exchange()
				.expectStatus().is4xxClientError();
	}

	@Test
	@Order(402)
	void updateCustomerWithInvalidId() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");

		CustomerRequest updateRequest = new CustomerRequest();
		updateRequest.setName(createResponse.getName());
		updateRequest.setNumber(createResponse.getNumber());
		updateRequest.setAddress(createResponse.getAddress());

		webTestClient.put().uri(CustomerController.UPDATE_CUSTOMER_ENDPOINT, UUID.randomUUID()).bodyValue(updateRequest)
				.exchange().expectStatus().isNoContent();
	}

	@Test
	@Order(500)
	void deleteCustomer() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");

		webTestClient.delete().uri(CustomerController.DELETE_CUSTOMER_ENDPOINT, createResponse.getId()).exchange()
				.expectStatus().is2xxSuccessful();
	}

	@Test
	@Order(501)
	void deleteCustomerAgain() {
		CustomerResponse createResponse = (CustomerResponse) testCache.get("createResponse");

		webTestClient.delete().uri(CustomerController.DELETE_CUSTOMER_ENDPOINT, createResponse.getId()).exchange()
				.expectStatus().isNoContent();
	}
}