package lb.ahmad.customer.management.api;

import org.testcontainers.containers.PostgreSQLContainer;

/**
 * Postgres test container used to create and configure a docker postgres container for integration testing during builds
 */
public class PostgresTestContainer extends PostgreSQLContainer<PostgresTestContainer>
{

	public static final String IMAGE_VERSION = "postgres:15.1-alpine";
	public static final String DATABASE_NAME = "postgres";
	public static final String DATABASE_USERNAME = "postgres";
	public static final String DATABASE_PASSWORD = "postgres";
	public static PostgreSQLContainer<PostgresTestContainer> container;

	private PostgresTestContainer()
	{
		super(IMAGE_VERSION);
	}

	public static PostgreSQLContainer<PostgresTestContainer> getInstance()
	{
		if(container == null)
		{
			container = new PostgresTestContainer().withDatabaseName(DATABASE_NAME).withUsername(DATABASE_USERNAME).withPassword(DATABASE_PASSWORD);
		}
		return container;
	}

	/**
	 * Start the container and set related environment variables
	 */
	@Override
	public void start()
	{
		super.start();
		System.setProperty("JDBC_HOST", getHost());
		System.setProperty("JDBC_PORT", getMappedPort(PostgresTestContainer.POSTGRESQL_PORT).toString());
		System.setProperty("JDBC_DATABASE", getDatabaseName());
		System.setProperty("DB_USERNAME", DATABASE_USERNAME);
		System.setProperty("DB_PASSWORD", DATABASE_PASSWORD);
	}
}

