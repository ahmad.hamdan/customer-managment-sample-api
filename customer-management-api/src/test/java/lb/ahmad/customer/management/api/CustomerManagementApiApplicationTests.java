package lb.ahmad.customer.management.api;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest(classes = CustomerManagementApiApplication.class)
class CustomerManagementApiApplicationTests {
	@Container
	private static final PostgreSQLContainer<PostgresTestContainer> postgreSQLContainer = PostgresTestContainer
			.getInstance();

	@Test
	void contextLoads() {
	}
}
