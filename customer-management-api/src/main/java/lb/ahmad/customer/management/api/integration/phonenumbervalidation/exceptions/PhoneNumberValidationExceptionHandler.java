package lb.ahmad.customer.management.api.integration.phonenumbervalidation.exceptions;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception Handler to handle Phone Number Validation exceptions before being
 * forwarded to an API response
 */
@Order(0)
@RestControllerAdvice
public class PhoneNumberValidationExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InvalidPhoneNumberException.class)
	public ErrorResponse invalidPhoneNumberException(InvalidPhoneNumberException ex) {
		ErrorResponse error = ErrorResponse.create(ex, HttpStatus.BAD_REQUEST, "Invalid phone number");
		return error;
	}
}
