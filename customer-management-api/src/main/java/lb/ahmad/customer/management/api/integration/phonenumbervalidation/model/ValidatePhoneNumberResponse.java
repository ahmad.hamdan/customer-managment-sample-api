package lb.ahmad.customer.management.api.integration.phonenumbervalidation.model;

/**
 * Response object used to model the response of the phone number validation api
 */
public class ValidatePhoneNumberResponse {
	private String operatorName;
	private String countryName;
	private String countryCode;

	public ValidatePhoneNumberResponse(String operatorName, String countryName, String countryCode) {
		super();
		this.operatorName = operatorName;
		this.countryName = countryName;
		this.countryCode = countryCode;
	}

	public ValidatePhoneNumberResponse() {
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	@Override
	public String toString() {
		return "ValidatePhoneNumberResponse{" + "operatorName='" + operatorName + '\'' + ", countryName='" + countryName
				+ '\'' + ", countryCode='" + countryCode + '\'' + '}';
	}
}
