package lb.ahmad.customer.management.api.integration.phonenumbervalidation;

import lb.ahmad.customer.management.api.integration.phonenumbervalidation.exceptions.InvalidPhoneNumberException;
import lb.ahmad.customer.management.api.integration.phonenumbervalidation.model.ValidatePhoneNumberResponse;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * {@link ValidatePhoneNumberService} implementation using number validation api
 */
@Service
public class ValidatePhoneNumberServiceImpl implements ValidatePhoneNumberService {
	private final String VALIDATE_PHONE_NUMBER_ENDPOINT;
	private final WebClient numberValidatoinApiWebClient;

	public ValidatePhoneNumberServiceImpl(
			@Value("${integrations.number-validation-api.validate-phone-number}") String validatePhoneNumberEndpoint) {
		numberValidatoinApiWebClient = WebClient.builder().build();
		VALIDATE_PHONE_NUMBER_ENDPOINT = validatePhoneNumberEndpoint;
	}

	@Override
	public Mono<ValidatePhoneNumberResponse> validatePhoneNumber(String number) {
		Mono<ValidatePhoneNumberResponse> mono = numberValidatoinApiWebClient.get()
				.uri(VALIDATE_PHONE_NUMBER_ENDPOINT, number).retrieve().onStatus((status) -> {
					System.out.println(status); // NO_CONTENT
					var result = status.isSameCodeAs(HttpStatus.NO_CONTENT);
					System.out.println(result); // TRUE
					return result;
				}, (response) -> {
					return Mono.error(new InvalidPhoneNumberException());
				}).bodyToMono(ValidatePhoneNumberResponse.class);
		return mono;
	}
}
