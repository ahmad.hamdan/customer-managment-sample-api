package lb.ahmad.customer.management.api.customer.model;

import java.util.Objects;
import java.util.UUID;

/**
 * Customer response object used to model the responses having a customer.
 */
public class CustomerResponse {
	private UUID id;
	private String name;
	private String address;
	private String number;

	public CustomerResponse() {
	}

	public CustomerResponse(Customer dao) {
		setId(dao.getId());
		setName(dao.getName());
		setAddress(dao.getAddress());
		setNumber(dao.getNumber());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CustomerResponse that = (CustomerResponse) o;
		return Objects.equals(name, that.name) && Objects.equals(address, that.address)
				&& Objects.equals(number, that.number);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, address, number);
	}

	@Override
	public String toString() {
		return "CustomerResponse{" + "id=" + id + ", name='" + name + '\'' + ", address='" + address + '\''
				+ ", number='" + number + '\'' + '}';
	}
}
