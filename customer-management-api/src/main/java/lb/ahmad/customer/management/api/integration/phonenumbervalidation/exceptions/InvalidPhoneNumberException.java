package lb.ahmad.customer.management.api.integration.phonenumbervalidation.exceptions;

/**
 * RuntimeException that rises when a phone number shown not valid
 */
public class InvalidPhoneNumberException extends RuntimeException {
}
