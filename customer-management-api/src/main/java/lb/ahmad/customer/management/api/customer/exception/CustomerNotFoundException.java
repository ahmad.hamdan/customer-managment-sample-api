package lb.ahmad.customer.management.api.customer.exception;

/**
 * RuntimeException that rises when a customer is not found
 */
public class CustomerNotFoundException extends RuntimeException {
}
