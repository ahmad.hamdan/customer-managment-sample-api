package lb.ahmad.customer.management.api.customer;

import lb.ahmad.customer.management.api.customer.exception.CustomerNotFoundException;
import lb.ahmad.customer.management.api.customer.model.Customer;
import lb.ahmad.customer.management.api.integration.phonenumbervalidation.ValidatePhoneNumberService;
import reactor.core.publisher.Mono;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * Customer Service Implementation
 */
@Service
public class CustomerServiceImpl implements CustomerService {
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

	private final CustomerRepository customerRepository;
	private final ValidatePhoneNumberService validatePhoneNumberService;

	public CustomerServiceImpl(CustomerRepository customerRepository,
			ValidatePhoneNumberService validatePhoneNumberService) {
		this.customerRepository = customerRepository;
		this.validatePhoneNumberService = validatePhoneNumberService;
	}

	@Override
	public Mono<Customer> createCustomer(Customer customerDao) {
		String number = customerDao.getNumber();
		return validatePhoneNumberService.validatePhoneNumber(number).map(response -> {
			var result = customerRepository.save(customerDao);
			log.info("Customer created: {}", result);
			return result;
		});
	}

	@Override
	public Page<Customer> readCustomers(Customer example, Pageable pageable) {
		Page<Customer> result = customerRepository.findAll(Example.of(example), pageable);
		return result;
	}

	@Override
	public Customer readCustomer(UUID id) {
		Optional<Customer> optionalCustomerDao = customerRepository.findById(id);
		return optionalCustomerDao.orElseThrow(CustomerNotFoundException::new);
	}

	@Override
	public Mono<Customer> updateCustomer(Customer customerDao) {
		if (customerDao.getId() == null) {
			throw new IllegalArgumentException();
		}
		if (!customerRepository.existsById(customerDao.getId())) {
			throw new CustomerNotFoundException();
		}
		String number = customerDao.getNumber();
		return validatePhoneNumberService.validatePhoneNumber(number).map(response -> {
			var result = customerRepository.save(customerDao);
			log.info("Customer modified: {}", result);
			return result;
		});
	}

	@Override
	public void deleteCustomer(UUID id) {
		if (!customerRepository.existsById(id)) {
			throw new CustomerNotFoundException();
		}
		customerRepository.deleteById(id);
	}
}
