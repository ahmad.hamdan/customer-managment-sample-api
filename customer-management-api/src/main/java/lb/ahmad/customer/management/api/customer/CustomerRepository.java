package lb.ahmad.customer.management.api.customer;

import lb.ahmad.customer.management.api.customer.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Customer JPA Repository
 */
public interface CustomerRepository extends JpaRepository<Customer, UUID> {
}
