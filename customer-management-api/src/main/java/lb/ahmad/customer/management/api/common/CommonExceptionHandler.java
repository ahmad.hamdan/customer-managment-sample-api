package lb.ahmad.customer.management.api.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;

/**
 * Common Exception Handler to handle general exceptions before being forwarded
 * to an API response
 */
@RestControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {
	private final static Logger log = LoggerFactory.getLogger(CommonExceptionHandler.class);

	@ExceptionHandler(PropertyReferenceException.class)
	public ErrorResponse propertyReferenceException(Exception ex) {
		ErrorResponse error = ErrorResponse.create(ex, HttpStatus.BAD_REQUEST, ex.getLocalizedMessage());
		return error;
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> exception(Exception ex) {
		log.error("Unexpected exception", ex);
		return ResponseEntity.internalServerError().build();
	}
}
