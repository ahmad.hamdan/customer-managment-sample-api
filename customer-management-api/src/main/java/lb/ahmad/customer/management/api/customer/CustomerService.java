package lb.ahmad.customer.management.api.customer;

import lb.ahmad.customer.management.api.customer.model.Customer;
import reactor.core.publisher.Mono;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

/**
 * Customer Service definition
 */
public interface CustomerService {
	/**
	 * Create a customer
	 *
	 * @param customerDao DAO of the customer to be created
	 * @return the created customer DAO
	 */
	Mono<Customer> createCustomer(Customer customerDao);

	/**
	 * Read customers page
	 *
	 * @param example  example customer to filter customers by
	 * @param pageable properties of the page (size, offset, sort, etc.)
	 * @return Customer DAOs page
	 */
	Page<Customer> readCustomers(Customer example, Pageable pageable);

	/**
	 * Read customer
	 *
	 * @param id UUID of the customer to be retrieved
	 * @return Customer DAO
	 */
	Customer readCustomer(UUID id);

	/**
	 * Update customer using the DAO provided
	 *
	 * @param customerDao Customer DAO to update the customer from
	 * @return the updated customer DAO
	 */
	Mono<Customer> updateCustomer(Customer customerDao);

	/**
	 * Delete the customer having the id provided
	 *
	 * @param id UUID of the customer to be deleted
	 */
	void deleteCustomer(UUID id);
}
