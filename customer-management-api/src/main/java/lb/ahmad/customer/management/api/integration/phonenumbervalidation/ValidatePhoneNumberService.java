package lb.ahmad.customer.management.api.integration.phonenumbervalidation;

import lb.ahmad.customer.management.api.integration.phonenumbervalidation.model.ValidatePhoneNumberResponse;
import reactor.core.publisher.Mono;

/**
 * Validate phone number service definition
 */
public interface ValidatePhoneNumberService {
	/**
	 * Validates the phone number provided
	 *
	 * @param number phone number to be validated
	 * @return 
	 */
	Mono<ValidatePhoneNumberResponse> validatePhoneNumber(String number);
}
