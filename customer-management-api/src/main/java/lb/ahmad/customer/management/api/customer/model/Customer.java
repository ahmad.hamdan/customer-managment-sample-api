package lb.ahmad.customer.management.api.customer.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Objects;
import java.util.UUID;

/**
 * Customer Data Access Object to be used internally in the service and database
 * accesses.
 */
@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.UUID)
	private UUID id;
	private String name;
	private String address;
	private String number;

	public Customer() {
	}

	public Customer(CustomerRequest request) {
		setName(request.getName());
		setAddress(request.getAddress());
		setNumber(request.getNumber());
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Customer that = (Customer) o;
		return Objects.equals(name, that.name) && Objects.equals(address, that.address)
				&& Objects.equals(number, that.number);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, address, number);
	}

	@Override
	public String toString() {
		return "CustomerDao{" + "id=" + id + ", name='" + name + '\'' + ", address='" + address + '\'' + ", number='"
				+ number + '\'' + '}';
	}
}
