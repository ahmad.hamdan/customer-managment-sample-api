package lb.ahmad.customer.management.api.customer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lb.ahmad.customer.management.api.customer.model.Customer;
import lb.ahmad.customer.management.api.customer.model.CustomerRequest;
import lb.ahmad.customer.management.api.customer.model.CustomerResponse;
import reactor.core.publisher.Mono;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.UUID;

/**
 * Controller used to define customers API endpoints and map response, request
 * and DAOs.
 */
@Tag(name = "Customers API", description = "API for Customer CRUD operations")
@RestController
@RequestMapping(CustomerController.BASE_PATH)
public class CustomerController {
	static final String BASE_PATH = "/customers";
	static final String CREATE_CUSTOMER_ENDPOINT = "";
	static final String READ_CUSTOMERS_ENDPOINT = "";
	static final String READ_CUSTOMER_ENDPOINT = "/{id}";
	static final String UPDATE_CUSTOMER_ENDPOINT = "/{id}";
	static final String DELETE_CUSTOMER_ENDPOINT = "/{id}";

	private final CustomerService customerService;

	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@Operation(summary = "Creates a customer", responses = {
			@ApiResponse(responseCode = "201", description = "Resource created", content = @Content(schema = @Schema(implementation = CustomerResponse.class, description = "Created customer details"), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "400", description = "Bad request. Details in the response body", content = @Content(schema = @Schema(implementation = ProblemDetail.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })
	@PostMapping(CREATE_CUSTOMER_ENDPOINT)
	public Mono<ResponseEntity<CustomerResponse>> createCustomer(@RequestBody CustomerRequest request) {
		Customer customerRequestDao = new Customer(request);
		Mono<Customer> mono = customerService.createCustomer(customerRequestDao);
		return mono.map(dao -> new CustomerResponse(dao))
				.map(response -> ResponseEntity
						.created(URI.create(BASE_PATH + CREATE_CUSTOMER_ENDPOINT + '/' + response.getId().toString()))
						.body(response));
	}

	@Operation(summary = "Read a page of customers filtered by example", responses = {
			@ApiResponse(responseCode = "200", description = "Page of customers", useReturnTypeSchema = true),
			@ApiResponse(responseCode = "400", description = "Bad request. Details in the response body", content = @Content(schema = @Schema(implementation = ProblemDetail.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })
	@GetMapping(READ_CUSTOMERS_ENDPOINT)
	public ResponseEntity<Page<CustomerResponse>> readCustomers(CustomerRequest example, Pageable pageable) {
		Customer exampleDao = new Customer(example);
		Page<Customer> daoPage = customerService.readCustomers(exampleDao, pageable);
		Page<CustomerResponse> result = daoPage.map(CustomerResponse::new);
		return ResponseEntity.ok(result);
	}

	@Operation(summary = "Read one customer by ID", responses = {
			@ApiResponse(responseCode = "200", description = "Customer found", content = @Content(schema = @Schema(implementation = CustomerResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "204", description = "Customer not found", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "400", description = "Bad request. Details in the response body", content = @Content(schema = @Schema(implementation = ProblemDetail.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })
	@GetMapping(READ_CUSTOMER_ENDPOINT)
	public ResponseEntity<CustomerResponse> readCustomer(@PathVariable UUID id) {
		Customer customerResponseDao = customerService.readCustomer(id);
		CustomerResponse response = new CustomerResponse(customerResponseDao);
		return ResponseEntity.ok(response);
	}

	@Operation(summary = "Update one customer by ID", responses = {
			@ApiResponse(responseCode = "200", description = "Customer updated", content = @Content(schema = @Schema(implementation = CustomerResponse.class, description = "Updated customer"), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "204", description = "Customer not found", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "400", description = "Bad request. Details in the response body", content = @Content(schema = @Schema(implementation = ProblemDetail.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })
	@PutMapping(UPDATE_CUSTOMER_ENDPOINT)
	public Mono<ResponseEntity<CustomerResponse>> updateCustomer(@PathVariable UUID id,
			@RequestBody CustomerRequest request) {
		Customer customerRequestDao = new Customer(request);
		customerRequestDao.setId(id);
		Mono<Customer> mono = customerService.updateCustomer(customerRequestDao);
		return mono.map(CustomerResponse::new).map(ResponseEntity::ok);
	}

	@Operation(summary = "Delete one customer by ID", responses = {
			@ApiResponse(responseCode = "200", description = "Customer deleted", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "204", description = "Customer not found", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "400", description = "Bad request. Details in the response body", content = @Content(schema = @Schema(implementation = ProblemDetail.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })
	@DeleteMapping(DELETE_CUSTOMER_ENDPOINT)
	public ResponseEntity<Object> deleteCustomer(@PathVariable UUID id) {
		customerService.deleteCustomer(id);
		return ResponseEntity.ok().build();
	}
}
