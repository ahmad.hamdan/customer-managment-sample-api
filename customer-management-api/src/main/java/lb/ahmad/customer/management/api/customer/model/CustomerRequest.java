package lb.ahmad.customer.management.api.customer.model;

import java.util.Objects;

/**
 * Customer request object used to model the received requests.
 */
public class CustomerRequest {
	private String name;
	private String address;
	private String number;

	public CustomerRequest() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		CustomerRequest that = (CustomerRequest) o;
		return Objects.equals(name, that.name) && Objects.equals(address, that.address)
				&& Objects.equals(number, that.number);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, address, number);
	}

	@Override
	public String toString() {
		return "CustomerRequest{" + "name='" + name + '\'' + ", address='" + address + '\'' + ", number='" + number
				+ '\'' + '}';
	}
}
