#!/bin/bash

docker rm -f lb-ahmad-code-challenge-postgres
docker rm -f lb-ahmad-code-challenge-frontend
docker rm -f lb-ahmad-code-challenge-customer-management-api
docker rm -f lb-ahmad-code-challenge-number-validation-api

docker run -d -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres --name lb-ahmad-code-challenge-postgres postgres:15.1-alpine
docker run -d -p 8080:8080 --name lb-ahmad-code-challenge-number-validation-api lb-ahmad/code-challenge/number-validation-api
docker run -d -p 8090:8090 --add-host=host-gateway:host-gateway -e JDBC_HOST="host-gateway" -e NUMBER_VALIDATION_API="http://host-gateway:8080/phonenumbers/{number}" --name lb-ahmad-code-challenge-customer-management-api lb-ahmad/code-challenge/customer-management-api
docker run -d -p 3000:3000 --name lb-ahmad-code-challenge-frontend lb-ahmad/code-challenge/frontend

