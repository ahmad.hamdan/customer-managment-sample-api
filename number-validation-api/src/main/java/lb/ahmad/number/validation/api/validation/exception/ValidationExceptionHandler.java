package lb.ahmad.number.validation.api.validation.exception;

import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception Handler to handle Customer API exceptions before being forwarded to
 * an API response
 */
@Order(0)
@RestControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(InvalidPhoneNumberException.class)
	public ResponseEntity<ErrorResponse> invalidPhoneNumberException(InvalidPhoneNumberException ex) {
		return ResponseEntity.noContent().build();
	}
}
