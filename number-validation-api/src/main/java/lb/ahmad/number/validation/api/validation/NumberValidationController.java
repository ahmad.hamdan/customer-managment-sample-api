package lb.ahmad.number.validation.api.validation;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lb.ahmad.number.validation.api.validation.dto.ValidatePhoneNumberResponse;
import reactor.core.publisher.Mono;

@Tag(name = "Number Validation API", description = "API for validating phone numbers")
@RestController
@RequestMapping("/phonenumbers/")
public class NumberValidationController {
	private final NumberValidationService numberValidationService;

	public NumberValidationController(NumberValidationService numberValidationService) {
		this.numberValidationService = numberValidationService;
	}

	@Operation(summary = "Validate the phone number in the path", responses = {
			@ApiResponse(responseCode = "200", description = "Phone number is valid", content = @Content(schema = @Schema(implementation = ValidatePhoneNumberResponse.class, description = "Phone number details"), mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "204", description = "Phone number not found (not valid)", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)),
			@ApiResponse(responseCode = "500", description = "Unexpected internal server error", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)) })

	@GetMapping("/{number}")
	public Mono<ResponseEntity<ValidatePhoneNumberResponse>> validatePhoneNumber(@PathVariable String number) {
		return numberValidationService.validatePhoneNumber(number);
	}
}
