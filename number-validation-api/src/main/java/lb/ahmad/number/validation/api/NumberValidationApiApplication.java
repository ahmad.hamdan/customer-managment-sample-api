package lb.ahmad.number.validation.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumberValidationApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumberValidationApiApplication.class, args);
	}

}
