package lb.ahmad.number.validation.api.validation.dto;

public class PhoneNumberApiResponse
{
	private String carrier;
	private String countryName;
	private String numberCountryCode;

	public PhoneNumberApiResponse()
	{
	}

	public String getCarrier()
	{
		return carrier;
	}

	public void setCarrier(String carrier)
	{
		this.carrier = carrier;
	}

	public String getCountryName()
	{
		return countryName;
	}

	public void setCountryName(String countryName)
	{
		this.countryName = countryName;
	}

	public String getNumberCountryCode()
	{
		return numberCountryCode;
	}

	public void setNumberCountryCode(String numberCountryCode)
	{
		this.numberCountryCode = numberCountryCode;
	}

	@Override
	public String toString()
	{
		return "PhoneNumberApiResponse{" + "carrier='" + carrier + '\'' + ", countryName='" + countryName + '\'' + ", numberCountryCode='" +
				numberCountryCode + '\'' + '}';
	}
}
