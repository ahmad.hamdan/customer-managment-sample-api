package lb.ahmad.number.validation.api.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;

/**
 * Common Exception Handler to handle general exceptions before being forwarded
 * to an API response
 */
@RestControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {
	private final static Logger log = LoggerFactory.getLogger(CommonExceptionHandler.class);

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> exception(Exception ex) {
		log.error("Unexpected exception", ex);
		return ResponseEntity.internalServerError().build();
	}
}
