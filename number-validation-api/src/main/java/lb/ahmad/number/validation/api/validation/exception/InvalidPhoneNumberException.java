package lb.ahmad.number.validation.api.validation.exception;

/**
 * RuntimeException that rises when a phone number shown not valid
 */
public class InvalidPhoneNumberException extends RuntimeException {
}
