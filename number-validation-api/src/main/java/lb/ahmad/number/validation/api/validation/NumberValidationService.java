package lb.ahmad.number.validation.api.validation;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import lb.ahmad.number.validation.api.validation.dto.PhoneNumberApiResponse;
import lb.ahmad.number.validation.api.validation.dto.ValidatePhoneNumberResponse;
import lb.ahmad.number.validation.api.validation.exception.InvalidPhoneNumberException;
import reactor.core.publisher.Mono;

@Service
public class NumberValidationService {
	private final String PHONE_NUMBER_API_BASE_URL = "http://phone-number-api.com/json?number={number}";
	private final WebClient phoneNumberWebClient;

	public NumberValidationService() {
		phoneNumberWebClient = WebClient.builder().build();
	}

	public Mono<ResponseEntity<ValidatePhoneNumberResponse>> validatePhoneNumber(String number) {
		WebClient.ResponseSpec retrieve = phoneNumberWebClient.get().uri(PHONE_NUMBER_API_BASE_URL, number).retrieve();
		Mono<PhoneNumberApiResponse> apiResponseMono = retrieve.bodyToMono(PhoneNumberApiResponse.class);
		Mono<ValidatePhoneNumberResponse> responseMono = apiResponseMono.map(ValidatePhoneNumberResponse::new);
		return responseMono.map(response -> {
			if(response.getCountryCode()==null)
			{
				throw new InvalidPhoneNumberException();
			} 
			return ResponseEntity.ok(response);
		});
	}	
}
