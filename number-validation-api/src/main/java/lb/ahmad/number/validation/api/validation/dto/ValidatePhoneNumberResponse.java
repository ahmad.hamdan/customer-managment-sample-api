package lb.ahmad.number.validation.api.validation.dto;

public class ValidatePhoneNumberResponse
{
	private String operatorName;
	private String countryName;
	private String countryCode;

	public ValidatePhoneNumberResponse()
	{
	}

	public ValidatePhoneNumberResponse(PhoneNumberApiResponse phoneNumberApiResponse)
	{
		this.operatorName = phoneNumberApiResponse.getCarrier();
		this.countryName = phoneNumberApiResponse.getCountryName();
		this.countryCode = phoneNumberApiResponse.getNumberCountryCode();
	}

	public String getOperatorName()
	{
		return operatorName;
	}

	public void setOperatorName(String operatorName)
	{
		this.operatorName = operatorName;
	}

	public String getCountryName()
	{
		return countryName;
	}

	public void setCountryName(String countryName)
	{
		this.countryName = countryName;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	@Override
	public String toString()
	{
		return "ValidatePhoneNumberResponse{" + "operatorName='" + operatorName + '\'' + ", countryName='" + countryName + '\'' + ", countryCode='" +
				countryCode + '\'' + '}';
	}
}
