package lb.ahmad.numbervalidationapi.validation;

import lb.ahmad.number.validation.api.NumberValidationApiApplication;
import lb.ahmad.number.validation.api.validation.dto.ValidatePhoneNumberResponse;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(classes = {
		NumberValidationApiApplication.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class NumberValidationControllerTest {
	@LocalServerPort
	Integer port;
	WebTestClient webTestClient;

	@BeforeEach
	void init() {
		webTestClient = WebTestClient.bindToServer().baseUrl("http://localhost:" + port).build();
	}

	@Test
	void validatePhoneNumberWithValidNumber() {
		String number = "96171509786";
		var responseBody = webTestClient.get().uri("/phonenumbers/{number}", number).exchange()
				.expectBody(ValidatePhoneNumberResponse.class).returnResult().getResponseBody();
		Assertions.assertNotNull(responseBody);
		Assertions.assertNotNull(responseBody.getCountryCode());
		Assertions.assertNotNull(responseBody.getCountryName());
		Assertions.assertNotNull(responseBody.getOperatorName());
	}

	@Test
	void validatePhoneNumberWithInvalidNumber() {
		String number = "9617150978";
		webTestClient.get().uri("/phonenumbers/{number}", number).exchange().expectStatus().isNoContent();
	}
}